$(document).ready(function(){
    $("#play-pause a").on('click', function() {
        if(jQuery(this).children('.glyphicon').hasClass('glyphicon-pause')) {
            jQuery(this).children('.glyphicon').addClass('glyphicon-play').removeClass('glyphicon-pause');
            jQuery(this).attr('title','Lecture');
            jQuery(this).children('.sr-only').text('Lecture');
            jQuery('#carousel-actus').carousel('pause');
        } else {
            jQuery(this).children('.glyphicon').addClass('glyphicon-pause').removeClass('glyphicon-play');
            jQuery(this).attr('title','Pause');
            jQuery(this).children('.sr-only').text('Pause');
            jQuery('#carousel-actus').carousel('cycle');
        };
        return false;
    });

    $('#info').not('.active').hide();
    $('#download').not('.active').hide();
    $('a[href*=info]').on('click', function() {
        $('#download').hide();
        $('#info').slideDown();
        return false;
    });
    $('a[href*=download]').on('click', function() {
        $('#info').hide();
        $('#download').slideDown();
        return false;
    });
});
